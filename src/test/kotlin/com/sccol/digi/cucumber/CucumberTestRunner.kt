package com.sccol.digi.cucumber

import io.cucumber.junit.CucumberOptions
import net.serenitybdd.cucumber.CucumberWithSerenity
import org.junit.runner.RunWith

@RunWith(CucumberWithSerenity::class)
@CucumberOptions(
    features = ["src/test/resources/features/crud_product.feature"],
    plugin = ["pretty"])
class CucumberTestRunner {}