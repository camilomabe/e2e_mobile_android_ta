package com.sccol.digi.cucumber


import com.sccol.digi.screenplay.tasks.AddNewProduct
import com.sccol.digi.screenplay.tasks.DeleteSavedProduct
import com.sccol.digi.screenplay.tasks.ModifyProduct
import com.sccol.digi.screenplay.tasks.VerifySavedProduct
import io.cucumber.java.Before
import io.cucumber.java.en.And
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnStage.theActorCalled
import net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Step


class AddNewProductStepDefinitions {

    @Before
    fun prepareStage() {
        OnStage.setTheStage(OnlineCast())
    }

    @Step("User opens the app {0}")
    @Given("^(.*) is a (.*)$")
    fun user_opens_the_app(username: String, description: String) {
        val actor = theActorCalled(username).describedAs(description)
        println("$actor is a $description")
    }

    @Step("add a new product {0}")
    @When("^(?:she|he) wants to add a new product$")
    fun add_new_product() {
        theActorInTheSpotlight().wasAbleTo(AddNewProduct.intoTheCart())
    }

    @Step("Verify the saved product {0}")
    @And("^(?:she|he) wants to verify the saving product$")
    fun verify_saved_product() {
        theActorInTheSpotlight().wasAbleTo(VerifySavedProduct.inShoppingCart())
    }

    @Step("Modify the saved product {0}")
    @And("^(?:she|he) modifies the product name$")
    fun modify_saved_product() {
        theActorInTheSpotlight().wasAbleTo(ModifyProduct.savedInCart())
    }

    @Step("Delete the saved product {0}")
    @Then("^(?:she|he) doesn't want the product and removes it$")
    fun delete_saved_product() {
        theActorInTheSpotlight().wasAbleTo(DeleteSavedProduct.inCart())
    }
}