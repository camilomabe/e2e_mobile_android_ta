Feature: Add product in shopping cart

  In this scenario, a product is added, modified, and deleted

  @addNewProduct
  Scenario: User wants to register a new product
    Given Alejandra is a client who opens the application
    When she wants to add a new product
    And she wants to verify the saving product
    And she modifies the product name
    Then she doesn't want the product and removes it


