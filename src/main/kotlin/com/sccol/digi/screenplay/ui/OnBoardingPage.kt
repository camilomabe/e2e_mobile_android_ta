package com.sccol.digi.screenplay.ui

import net.serenitybdd.core.annotations.findby.By
import net.serenitybdd.screenplay.targets.Target

object OnBoardingPage {
    const val APP_PACKAGE = "com.teamta.onboardingta:id/"
    @JvmField
    val LABEL_HOME = Target.the("Welcome to the Test Automation").located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.TextView"))
    @JvmField
    val ADD_PRODUCTS = Target.the("the plus icon to add a new product").located(By.id(APP_PACKAGE + "addProduct"))
    @JvmField
    val TXT_PRODUCT = Target.the("the product name text box").located(By.id(APP_PACKAGE + "txtProduct"))
    @JvmField
    val TXT_PRICE_PRODUCT = Target.the("the product price text box").located(By.id(APP_PACKAGE + "txtPriceProduct"))
    @JvmField
    val TXT_DESCRIPTION = Target.the("the product description text box").located(By.id(APP_PACKAGE + "txtDescription"))
    @JvmField
    val TXT_SUPERMARKET = Target.the("the grocery store text box").located(By.id(APP_PACKAGE + "txtSupermarket"))
    @JvmField
    val BTN_SAVE = Target.the("the save button").located(By.id(APP_PACKAGE + "btnSave"))
    @JvmField
    val VIEW_NAME_SAVED = Target.the("the description product").located(By.id(APP_PACKAGE + "viewNameSaved"))
    @JvmField
    val VIEW_PRODUCT_NAME = Target.the("the label shows the name of the saved product").located(By.id(APP_PACKAGE + "viewProductName"))
    @JvmField
    val VIEW_PRICE_PRODUCT = Target.the("the label shows the price of the saved product").located(By.id(APP_PACKAGE + "viewPriceProduct"))
    @JvmField
    val VIEW_DESCRIPTION_PRODUCT = Target.the("the label shows the description saved product").located(By.id(APP_PACKAGE + "viewDescriptionProduct"))
    @JvmField
    val IMG_PRODUCT = Target.the("Saved product image").located(By.id(APP_PACKAGE + "imgProduct"))
    @JvmField
    val TXT_EDIT_ITEM = Target.the("the edit icon").located(By.id(APP_PACKAGE + "txtEditItem"))
    @JvmField
    val DELETE_ITEM = Target.the("the delete icon").located(By.id(APP_PACKAGE + "txtDeleteItem"))
}