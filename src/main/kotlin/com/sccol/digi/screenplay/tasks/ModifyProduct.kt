package com.sccol.digi.screenplay.tasks


import com.sccol.digi.screenplay.data.DataUser
import com.sccol.digi.screenplay.ui.OnBoardingPage
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.Tasks.instrumented
import net.serenitybdd.screenplay.actions.Click
import net.serenitybdd.screenplay.actions.type.Type
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers
import net.serenitybdd.screenplay.waits.WaitUntil

open class ModifyProduct : Task {
    override fun <T : Actor> performAs(actor: T) {
        actor.wasAbleTo(
            WaitUntil.the(OnBoardingPage.TXT_EDIT_ITEM,
                WebElementStateMatchers.isVisible()).forNoMoreThan(15).seconds()
        )
        actor.attemptsTo(
            Click.on(OnBoardingPage.TXT_EDIT_ITEM),
            Type.theValue(DataUser.modifyProductName).into(OnBoardingPage.TXT_PRODUCT),
            Type.theValue(DataUser.modifyProductPrice.toString()).into(OnBoardingPage.TXT_PRICE_PRODUCT),
            Type.theValue(DataUser.modifyProductDescription).into(OnBoardingPage.TXT_DESCRIPTION),
            Type.theValue(DataUser.modifyGroceryStore).into(OnBoardingPage.TXT_SUPERMARKET),
            Click.on(OnBoardingPage.BTN_SAVE)
        )
    }

    companion object {
        fun savedInCart(): ModifyProduct {
            return instrumented(ModifyProduct::class.java)
        }
    }
}