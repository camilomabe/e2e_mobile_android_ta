package com.sccol.digi.screenplay.tasks

import com.sccol.digi.screenplay.data.DataUser
import com.sccol.digi.screenplay.ui.OnBoardingPage
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.Tasks.instrumented
import net.serenitybdd.screenplay.actions.Click
import net.serenitybdd.screenplay.actions.type.Type
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers
import net.serenitybdd.screenplay.waits.WaitUntil

open class AddNewProduct : Task {
    override fun <T : Actor> performAs(actor: T) {
        actor.wasAbleTo(
            WaitUntil.the(OnBoardingPage.ADD_PRODUCTS,
                WebElementStateMatchers.isVisible()).forNoMoreThan(15).seconds()
        )

        actor.attemptsTo(
            Click.on(OnBoardingPage.ADD_PRODUCTS),
            Type.theValue(DataUser.productName).into(OnBoardingPage.TXT_PRODUCT),
            Type.theValue(DataUser.productPrice.toString()).into(OnBoardingPage.TXT_PRICE_PRODUCT),
            Type.theValue(DataUser.productDescription).into(OnBoardingPage.TXT_DESCRIPTION),
            Type.theValue(DataUser.groceryStore).into(OnBoardingPage.TXT_SUPERMARKET),
            Click.on(OnBoardingPage.BTN_SAVE)
        )
    }

    companion object {
        fun intoTheCart(): AddNewProduct {
            return instrumented(AddNewProduct::class.java)
        }
    }
}