package com.sccol.digi.screenplay.tasks

import com.sccol.digi.screenplay.ui.OnBoardingPage
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.Tasks
import net.serenitybdd.screenplay.actions.Click
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers
import net.serenitybdd.screenplay.waits.WaitUntil

open class VerifySavedProduct : Task {
    override fun <T : Actor> performAs(actor: T) {
        actor.attemptsTo(
            WaitUntil.the(OnBoardingPage.VIEW_NAME_SAVED, WebElementStateMatchers.isVisible()).forNoMoreThan(15).seconds(),
            Click.on(OnBoardingPage.VIEW_NAME_SAVED),
            WaitUntil.the(OnBoardingPage.VIEW_PRODUCT_NAME, WebElementStateMatchers.isVisible()).forNoMoreThan(15).seconds(),
            WaitUntil.the(OnBoardingPage.VIEW_PRICE_PRODUCT, WebElementStateMatchers.isVisible()).forNoMoreThan(15).seconds(),
            WaitUntil.the(OnBoardingPage.VIEW_DESCRIPTION_PRODUCT, WebElementStateMatchers.isVisible()).forNoMoreThan(15).seconds(),
            WaitUntil.the(OnBoardingPage.IMG_PRODUCT, WebElementStateMatchers.isVisible()).forNoMoreThan(15).seconds()
        )
    }

    companion object {
        fun inShoppingCart(): VerifySavedProduct {
            return Tasks.instrumented(VerifySavedProduct::class.java)
        }
    }

}