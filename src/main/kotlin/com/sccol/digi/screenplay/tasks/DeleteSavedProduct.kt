package com.sccol.digi.screenplay.tasks


import com.sccol.digi.screenplay.ui.OnBoardingPage
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.Tasks.instrumented
import net.serenitybdd.screenplay.actions.Click
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers
import net.serenitybdd.screenplay.waits.WaitUntil

open class DeleteSavedProduct : Task {
    override fun <T : Actor> performAs(actor: T) {
        actor.wasAbleTo(
            WaitUntil.the(OnBoardingPage.DELETE_ITEM,
                WebElementStateMatchers.isVisible()).forNoMoreThan(15).seconds()
        )
        actor.attemptsTo(Click.on(OnBoardingPage.DELETE_ITEM))
    }

    companion object {
        fun inCart(): DeleteSavedProduct {
            return instrumented(DeleteSavedProduct::class.java)
        }
    }
}