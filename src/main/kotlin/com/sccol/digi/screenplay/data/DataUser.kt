package com.sccol.digi.screenplay.data

object DataUser {
    var productName = "Apple"
    var productDescription = "Grains and groceries"
    var productPrice = 1500
    var groceryStore = "Just & Good"

    var modifyProductName = "Pineapple"
    var modifyProductDescription = "Fruits and vegetables"
    var modifyProductPrice = 5000
    var modifyGroceryStore = "Of One"
}