package com.sccol.digi.screenplay.utils

import com.sccol.digi.screenplay.utils.environment.EnvironmentConstants
import net.serenitybdd.core.webdriver.enhancers.BeforeAWebdriverScenario
import net.thucydides.core.model.TestOutcome
import net.thucydides.core.util.EnvironmentVariables
import net.thucydides.core.webdriver.SupportedWebDriver
import org.openqa.selenium.remote.DesiredCapabilities

class ExtraCapabilities : BeforeAWebdriverScenario {
    override fun apply(
        environmentVariables: EnvironmentVariables,
        driver: SupportedWebDriver,
        testOutcome: TestOutcome,
        capabilities: DesiredCapabilities): DesiredCapabilities {
        capabilities.setCapability(EnvironmentConstants.GIGAFOX_USERNAME, environmentVariables.getProperty(EnvironmentConstants.GIGAFOX_USERNAME.replace(":", "_"), ""))
        capabilities.setCapability(EnvironmentConstants.GIGAFOX_APIKEY,environmentVariables.getProperty(EnvironmentConstants.GIGAFOX_APIKEY.replace(":", "_"), ""))
        capabilities.setCapability(EnvironmentConstants.GIGAFOX_DEVICE,environmentVariables.getProperty(EnvironmentConstants.GIGAFOX_DEVICE.replace(":", "_"), ""))
        capabilities.setCapability(EnvironmentConstants.GIGAFOX_SKIPINSTALL,environmentVariables.getProperty(EnvironmentConstants.GIGAFOX_SKIPINSTALL.replace(":", "_"), ""))
        capabilities.setCapability(EnvironmentConstants.APPIUM_HUB,  environmentVariables.getProperty(EnvironmentConstants.APPIUM_HUB, EnvironmentConstants.DEFAULT_APPIUM_URL))
        return capabilities
    }
}