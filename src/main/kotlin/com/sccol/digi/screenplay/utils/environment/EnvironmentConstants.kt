package com.sccol.digi.screenplay.utils.environment

object EnvironmentConstants {
    const val GIGAFOX_USERNAME = "gigafox:username"
    const val GIGAFOX_APIKEY = "gigafox:apiKey"
    const val GIGAFOX_DEVICE = "gigafox:device"
    const val GIGAFOX_SKIPINSTALL = "gigafox:skipInstall"
    const val APPIUM_HUB = "appium.hub"
    const val DEFAULT_APPIUM_URL = "http://localhost:4723/wd/hub"
}