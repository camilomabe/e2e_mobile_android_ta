## Onboarding TA

---

**1. Run configurations**


- Main class: `net.serenitybdd.cucumber.cli.Main`
- Glue: `net.serenity.cucumber.actor com.sccol.digi`
- Feature or folder path: `C:\git\e2e_mobile_ta_android\src\test\resources\features\login_app.feature`
- Program arguments: `--name "^User wants to register a new product$"`
- Use class path or module: `e2e_mobile_ta_android.test`
- JRE: `1.8`
- Shorten command line: `JAR manifest`
- Execute from command line IntelliJ: `gradle clean test aggregate` This command cleans, executes and adds the report

##

![Screenshot](src/documentation/RunConfig.png)

##

---

**2. Create class, objects, features and configuration files**


- `serenity.properties` add your GigaFox credentials from line 35 to 38 in this file
- Open IntelliJ and create a new project called `e2e_mobile_ta_android` in `C:\git` replace `build.gradle` and `serenity.properties` from this project in your new project
- `build.gradle` in build.gradle you must add the necessary dependencies indicated in the configuration file
- `serenity.properties` In serenity.properties are the configuration parameters necessary to run a test on mobile devices hosted on the GigaFox farm
- `crud_product.feature` define steps based on a user story for step-by-step execution
- `CucumberTestRunner.kt` this class executes the steps defined in the user story
- `DataUser.kt` define the values to create and modify product properties
- `OnBoardingPage.kt` in the object type file the web elements are mapped
- `AddNewProductStepDefinitions.kt` In this class the tasks are called, and they're executed according to the user story
- `AddNewProduct.kt` task to add a new product
- `VerifySavedProduct.kt` task to verify a product
- `ModifyProduct.kt` task to modify a product
- `DeleteSavedProduct.kt` task to remove a product
- `EnvironmentConstants.kt` in this object are the configuration parameters and connection to GigaFox
- `ExtraCapabilities.kt` tn this class, the connection parameters are modified, in this way they can be recognized by GigaFox and allow connection to mobile devices hosted in the farm.

##

---

**3. Create first test**


- Create a new file in as `src\test\resources\features\crud_product.feature`

```Gherkin
Feature: In this scenario, a product is added, modified, and deleted

  @addNewProduct
  Scenario: User wants to register a new product
    Given Alejandra is a client who opens the application
    When she wants to add a new product
    And she wants to verify the saving product
    And she modifies the product name
    Then she doesn't want the product and removes it
```

##

---

**4. Create the web elements page**


- `com.sccol.digi.screenplay.ui.OnBoardingPage.kt`

```Kotlin

package com.sccol.digi.screenplay.ui

import packages

object OnBoardingPage {

    val APP_PACKAGE = "com.teamta.onboardingta"
    @JvmField
    val ADD_PRODUCTS =  Target.the("the plus icon to add a new product").located(By.id(APP_PACKAGE + ":id/addProduct"))
}
```

##

---

**5. Create Runner class**

- `com\sccol\digi\cucumber\CucumberTestRunner.kt`

```Kotlin
package com.sccol.digi.cucumber

import packages

@RunWith(CucumberWithSerenity::class)
@CucumberOptions(features = ["src/test/resources/features/crud_product.feature"], plugin = ["pretty"])
    class CucumberTestRunner {
    }
```

##

---

**6. Create step definitions  class**

- `com\sccol\digi\cucumber\AddNewProductStepDefinitions.kt`

```Kotlin
package com.sccol.digi.cucumber

import packages

class LoginStepDefinitions {

    @Before
    fun prepareStage() {
        OnStage.setTheStage(OnlineCast())
    }
    @Step("User opens the app {0}")
    @Given("^(.*) is a (.*)$")
    fun user_opens_the_app(username: String, description: String) {
        val actor = theActorCalled(username).describedAs(description)
        println("$username is a $description")
    }

    @Step("add a new product {0}")
    @When("^(?:she|he) wants to add a new product$")
    fun add_new_product() {
        theActorInTheSpotlight().wasAbleTo(AddNewProduct.intoTheCart())
    }
}
```

##

---

**7. Create first task**

- `\src\main\kotlin\com\sccol\digi\screenplay\tasks\AddNewProduct.kt`

```Kotlin
package com.sccol.digi.screenplay.tasks

import packages

open class AddNewProduct : Task {

    override fun <T : Actor> performAs(actor: T) {
        actor.wasAbleTo(
            WaitUntil.the(
                OnBoardingPage.ADD_PRODUCTS,
                WebElementStateMatchers.isVisible()).forNoMoreThan(15).seconds()
        )

        actor.attemptsTo(
            Click.on(OnBoardingPage.ADD_PRODUCTS),
            Type.theValue(DataUser.productName).into(OnBoardingPage.TXT_PRODUCT),
            Type.theValue(DataUser.productPrice.toString()).into(OnBoardingPage.TXT_PRICE_PRODUCT),
            Type.theValue(DataUser.productDescription).into(OnBoardingPage.TXT_DESCRIPTION),
            Type.theValue(DataUser.groceryStore).into(OnBoardingPage.TXT_SUPERMARKET),
            Click.on(OnBoardingPage.BTN_SAVE)
        )
    }

    companion object {
        fun intoTheCart(): AddNewProduct {
            return instrumented(AddNewProduct::class.java)
        }
    }
}
```